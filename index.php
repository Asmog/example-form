
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Formulaire d'inscription</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">    <link rel="stylesheet" href="style.css">
</head>
<body class="darkBlue d-flex justify-content-center">
<?php 
$database = new SQLite3('db.sqlite');
?>
    <div class="rounded lightBlue d-flex justify-content-center mx-auto my-3 p-2">
        <form method="POST" id="usrform" action="./table.php" onsubmit="return validate()">
            <p class="fs-2 text text-center">Formulaire d'inscription</p>
            <hr>
            <div> 
                <label class="form-label" for="civil"><b>Civilité : </b></label><br>
                <select class="form-select form-select-lg" name="civil" id="civil-select">
                    <option value="Monsieur">Monsieur</option>
                    <option value="Madame">Madame</option>
                </select>
            </div>
            <div>
                <label class="form-label" for="nom"><b>Nom : </b></label><br>
                <input class="form-control" type="text" id="nom" placeholder="Nom" name="nom" required>
                <p id="regexNom" class="hidden text-danger"> Veuillez entrer un nom composé uniquement de lettre. </p>
            </div>
            <div>
                <label class="form-label" for="prenom"><b>Prénom : </b></label><br>
                <input class="form-control" type="text" id="prenom" placeholder="Prénom" name="prenom" required>
                <p id="regexPrenom" class="hidden text-danger"> Veuillez entrer un prénom composé uniquement de lettre. </p>
            </div>
            <div> 
                <label class="form-label" for="mail"><b>E-Mail : </b></label><br>
                <input class="form-control" type="text" id="mail" placeholder="E-Mail" name="mail" required>
                <p id="regexMail" class="hidden text-danger"> Veuillez entrer un email valide. </p>

            </div>
            <div>
                <label class="form-label" for="phone" name="phone"><b>Téléphone : </b></label><br>
                <input class="form-control" type="tel" id="phone" placeholder="Numéro de Téléphone" name="phone" required>
                <p id="regexPhone" class="hidden text-danger">Veuillez entrer un numéro valide : +33678912345 ou 0123456789</p>
            </div>
            <div>
                <label class="form-label" for="birth" name="birth"><b>Date de naissance : </b></label><br>
                <input class="form-control" type="date" value=<?php echo date("Y-m-d"); ?> name="birth" min="1923-01-01" max=<?php echo date("Y-m-d"); ?> required>
            </div>
            <br>
            <div class="form-floating">
                <textarea class="form-control" id="comment" form="usrform" maxlength=255></textarea>
                <label placeholder="Commentez ici" for="comment" >Commentaire (max : 255 caractères): </label><br>
            </div>
            <div >
                <input class="form-check-input" type="checkbox" name="RGPD" required>
                <label class="form-check-label" for="RGPD">en soumettant ce formulaire, j'accepte que les informations saisies <br>soient exploitées dans le cadre du site et de la relation commercial <br>qui peut en découler, sous conformité au RGPD.</label>
            </div>
            <div class="d-flex justify-content-end">
                <button class="btn btn-light" type="submit" name="submit" value="Envoyer">Envoyer</button>
            </div>
        </form>

    </div>
    <script src="script.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.min.js" integrity="sha384-Rx+T1VzGupg4BHQYs2gCW9It+akI2MM/mndMCy36UVfodzcJcF0GGLxZIzObiEfa" crossorigin="anonymous"></script></body>
</html>
