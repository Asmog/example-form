function validate()
{ 
  /* réinitialisation des messages d'erreur */
  document.getElementById('regexNom').classList.add("hidden");
  document.getElementById('regexPrenom').classList.add("hidden");
  document.getElementById('regexPhone').classList.add("hidden");
  document.getElementById('regexMail').classList.add("hidden");

  /* Récupération des valeurs de chaque champs*/
  var nom = document.getElementById('nom').value;
  var prenom = document.getElementById('prenom').value;
  var mail = document.getElementById('mail').value;
  var phone = document.getElementById('phone').value;
  /* Création de Regex */
  var letterRegex = /^[a-zA-Z]+$/;
  var mailRegex = /^\w+[!"#$%&'()*+,\-./:;<=>?@[\]^_`{|}~]*\w*@\w+\.\w+$/;
  var phoneRegex = /^(\+33|0|0033)[1-9]\d{8}$/;
  /* Application des regex au champs associés */
  var nomResult = letterRegex.test(nom);
  var prenomResult = letterRegex.test(prenom);
  var mailResult = mailRegex.test(mail);
  var phoneResult = phoneRegex.test(phone);

  /* Si le Regex est mauvais, 
  affichage d'un message à l'utilisateur 
  et prévention de l'envoie du formulaire*/
  
  var good = true;
    if (nomResult == false){
      document.getElementById('regexNom').classList.remove("hidden");
      good = false
    }
    if (prenomResult == false){
      document.getElementById('regexPrenom').classList.remove("hidden");
      good = false
    }
    if (mailResult == false){
      document.getElementById('regexMail').classList.remove("hidden");
      good = false
    }
    if (phoneResult == false){
      document.getElementById('regexPhone').classList.remove("hidden");
      good = false
    }
    return good;
  }
