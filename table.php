
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Inscription prise en compte</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">    <link rel="stylesheet" href="style.css">
</head>
<body class="darkBlue flexColCenter">
  <?php
    /* Connexion BDD */
    $db = new SQLite3('db.sqlite');
    /*Verification existence Data pour insertion BDD */
    if(isset($_POST["prenom"]) && isset($_POST["nom"]) 
    && isset($_POST["civil"]) && isset($_POST["mail"]) 
    && isset($_POST["phone"]) && isset($_POST["birth"]) 
    && isset($_POST["comment"]) ){
      /*Preparation Requete + Execution */
      $stmt = $db->prepare('INSERT INTO user (civil, nom, prenom, mail, phone, birth, comment) VALUES (:civil, :nom, :prenom, :mail, :phone, :birth, :comment)');
      $stmt->bindValue(':civil', $_POST["civil"]);
      $stmt->bindValue(':nom', $_POST["nom"]);
      $stmt->bindValue(':prenom', $_POST["prenom"]);
      $stmt->bindValue(':mail', $_POST["mail"]);
      $stmt->bindValue(':phone', $_POST["phone"]);
      $stmt->bindValue(':birth', $_POST["birth"]);
      $stmt->bindValue(':comment', $_POST["comment"]);

      $result = $stmt->execute();
    }
    /* verification parametre url pour supprimer ligne */
    if(isset($_GET["del"])){
      /* Preparation puis execution de la suppression */
      $stmt = $db->prepare("DELETE FROM 'user' WHERE id=:id");
      $stmt->bindValue(':id', $_GET["del"]);
      $result = $stmt->execute();
    }
    ?>
   
   <p>Tableau récapitualtif de la BDD</p>
   <br>
   <table class="table table-striped">
     <thead>
       <tr class='text-center'>
         <th scope="col">Civilité</th>
         <th scope="col">Nom</th>
         <th scope="col">Prénom</th>
         <th scope="col">Email</th>
         <th scope="col">Téléphone</th>
         <th scope="col">Date de naissance</th>
         <th scope="col">Commentaire</th>
         <th scope="col">Supprimer</th>
        </tr>
      </thead>
      <tbody>
      <?php
        /* Récupération données BDD */
        $data = $db->query('SELECT * FROM user');
        if ($data) {
          while ($row = $data->fetchArray()) {
              /* Affichage données en mode Tableau */
              echo "<tr class='text-center'>";
              echo "<td>".$row["civil"]."</td>";
              echo "<td>".$row["nom"]."</td>";
              echo "<td>".$row["prenom"]."</td>";
              echo "<td>".$row["mail"]."</td>";
              echo "<td>".$row["phone"]."</td>";
              echo "<td>".$row["birth"]."</td>";
              /* Si la section Commentaire est vide affichage d'un message */
              if ($row["comment"] == ""){
                echo "<td> N/A </td>";
              }
              else {
              echo "<td>".$row["comment"]."</td>";
              }
              /* Bouton d'action de Suppression de la ligne dans la BDD */
              echo "<td><a href='?del=".$row["id"]."'><button class='btn btn-danger text-light'>X</button></a></td>";
              echo "</tr>";
          }
        }
      ?>
      </tbody>
    </table>
  <br>
  <a href="./index.php"><button class="btn btn-light">Retour</button></a>
  <script src="script.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.min.js" integrity="sha384-Rx+T1VzGupg4BHQYs2gCW9It+akI2MM/mndMCy36UVfodzcJcF0GGLxZIzObiEfa" crossorigin="anonymous"></script></body>
</html>
